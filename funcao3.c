double subtracao_entradas(double vetor[], int n){
	double sub = 0;
	double aux = 0;
	
	for(int i = 0; i < n; i++){
		aux = vetor[i];
		sub = sub - aux;
	};
	
	return sub;
}
