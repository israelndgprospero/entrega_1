#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "funcao_1.h"
#include "funcao_2.h"
#include "funcao_3.h"

int main(void){
	
	int n = 10;
	double vetor[n];
	
	srand(time(NULL));
	
	for(int i = 0; i < n; i++){
		int rand_int = rand();
		
		double rand_double = (double)rand_int*10/RAND_MAX;
		
		// printf("%lf\n", rand_double);
		
		vetor[i] = rand_double;
	}
	
	double soma;
	soma = soma_entradas(vetor, n);
	printf("%lf \n", soma);
	
	double mult;
	mult = multiplicacao_entradas(vetor, n);
	printf("%lf \n", mult);
	
	double sub;
	sub = subtracao_entradas(vetor, n);
	printf("%lf \n", sub);
}
