double multiplicacao_entradas(double vetor[], int n){
	double mult = 1;
	double aux = 0;
	
	for(int i = 0; i < n; i++){
		aux = vetor[i];
		mult = mult * aux;
	};
	
	return mult;
}

