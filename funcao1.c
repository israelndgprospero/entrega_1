double soma_entradas(double vetor[], int n){
	double soma = 0;
	double aux = 0;
	
	for(int i = 0; i < n; i++){
		aux = vetor[i];
		soma = soma + aux;
	};
	
	return soma;
}
