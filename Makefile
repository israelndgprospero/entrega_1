ALL = main

all: $(ALL)

main: main.o funcao_1.o funcao_2.o funcao_3.o
	gcc -o $@ $^ -lm
	
%.o: %.c
	gcc -c $<
	
clean:
	rm -f *.s *.o

distclean: clean
	rm -f (ALL)
